# Contributing to the congrats-bot repository

This repository contains a [gidgetlab](https://pypi.org/project/gidgetlab/) bot which sends a message of congratulations on a Slack channel, whenever an MR on [GitLab](https://gitlab.com/) has been closed.

[[_TOC_]]

---

## Getting started

### Clone this repository

From the command line, run:

```sh
git clone git@gitlab.com:beautifulcanoe/peopleops/congrats-bot.git
cd congrats-bot
```

### Installing dependencies

Firstly, install [virtualenv](https://docs.python-guide.org/dev/virtualenvs/) and create a new virtual environment.
This is a new environment with its own version of Python and its own package manager, so we can ensure that whatever we install here is exactly as it will be on the GitLab servers:

```sh
sudo apt-get install python3-virtualenv
virtualenv --python=python3.10 venv
. venv/bin/activate
```

Your command line prompt should now look like this:

```sh
(venv) $
```

If you need to get out of the virtual environment, run the `deactivate` command.

Next, install the Python requirements:

```sh
pip install -r requirements.txt
```

Depending on your environment, you may need to use `pip3` rather than `pip` here.

## Linting your code

Python code in this repository should pass both [pycodestyle](https://pypi.org/project/pycodestyle/) and [pylint](https://www.pylint.org/).
Both packages are in the `requirements.txt` file, and so do not need to be installed separately.

### Ensuring that your Markdown syntax is valid

To make sure that your Markdown is valid, please use [Markdown lint](https://github.com/markdownlint/markdownlint).

To install the lint on Debian-like machines, use the [Rubygems](https://rubygems.org/) package manager:

```sh
sudo apt-get install gem
sudo gem install mdl
```

!!! Warning
    If you are using Ubuntu 18.04, you may need to install the `rubygems` package rather than `gem`.

Because we keep each sentence on a separate line, you will want to suppress spurious `MD013 Line length` reports by configuring `mdl`.
The file `/.mdl.rb` contains styles that deal with `MD013` and other tweaks we want to make to the lint.
To use the style configuration, pass it as a parameter to `mdl` on the command line:

```sh
mdl -s .mdl.rb README.md
```

If you want to run `mdl` from your IDE or editor, you will either need to configure it, or find a plugin, such as [this one for Sublime Text](https://github.com/SublimeLinter/SublimeLinter-mdl).

## Setting up a git hook

This repository provides [git hooks](https://githooks.com/) that will run `mdl`, `pycodestyle` and `pylint` each time the developer commits their code, and refuse to perform the commit if the changes do not pass the lint.
When `git push` is run, another hook will check that the site can be correctly built.

To install the hooks, first ensure that you have `mdl` and `pandoc` installed correctly, and that you can build the site.
Next, run the hook install script:

```sh
./bin/create-hook-symlinks
```

## Changing the layout of the Slack post

The Slack post is in [block](https://api.slack.com/block-kit) format.
If you want to experiment with blocks, without actually posting to a Slack channel, there is an interactive block builder [available in the browser](https://app.slack.com/block-kit-builder/T1GLYGEQ1).

## Further reading

* [Block builder kit](https://api.slack.com/block-kit)
* [gidgetlab](https://pypi.org/project/gidgetlab/)
* [git hooks](https://githooks.com/)
* [GitLab](https://gitlab.com/)
* [Heroku](https://dashboard.heroku.com/)
* [Markdown lint](https://github.com/markdownlint/markdownlint).
* [Markdown syntax](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/)
* [Python virtualenv](https://docs.python-guide.org/dev/virtualenvs/)
* [pycodestyle](https://pypi.org/project/pycodestyle/)
* [pylint](https://www.pylint.org/)
