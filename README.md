# Bob the Congrats Bot

[![pipeline status](https://gitlab.com/beautifulcanoe/peopleops/congrats-bot/badges/main/pipeline.svg)](https://gitlab.com/beautifulcanoe/peopleops/congrats-bot/-/commits/main)
[![MIT license](https://img.shields.io/badge/license-MIT-blue)](https://choosealicense.com/licenses/mit/)
[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy)

Bob the Congrats Bot congratulates developers on Slack when their MRs are merged.

## CI keys

This repository requires three CI variables for deploying Bob on [Heroku](https://dashboard.heroku.com/):

* `HEROKU_API_KEY` which is an API key for the user who owns the apps,
* `HEROKU_APP_STAGING` the name of a staging app; and
* `HEROKU_APP_PRODUCTION` the name of a production app.

The Heroku apps require three environment variables:

* `PORT` which is set automatically by Heroku,
* `SLACK_POST_HOOK` which should be a [webhook](https://api.slack.com/tutorials/slack-apps-hello-world) for posting to your Slack channel,
* `GIPHY_API_TOKEN` which should be an [API key](https://support.giphy.com/hc/en-us/articles/360020283431-Request-A-GIPHY-API-Key) for Giphy; and
* `GITLAB_API_TOKEN` which should be a GitLab Personal Access Token.
* `LOG_LEVEL` which should be either `ERROR`, `DEBUG`, `WARN`, or `INFO`.

## Making changes to Bob the Congrats Bot

Please see [CONTRIBUTING.md](/CONTRIBUTING.md) for advice on how to contribute to this repository.

## License

This work is licensed under the [MIT license](/LICENSE.md)

