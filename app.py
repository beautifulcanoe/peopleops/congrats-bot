"""
Congrats bot!

Listen for GitLab events and post a message of congratulations on Slack when a
developer has an MR merged.

Requires environment variables:

* PORT - for HTTP connections,
* GIPHY_API_TOKEN - API key for Giphy,
* GITLAB_API_TOKEN - personal access token,
* LOG_LEVEL - ERROR, DEBUG, WARN, or INFO,
* SLACK_POST_HOOK - a webhook for your desired Slack channel.
"""

import json
import logging
import os
import random

import giphy_client
from giphy_client.rest import ApiException
import gitlab
import requests

from gidgetlab.aiohttp import GitLabBot


BOT = GitLabBot('bc-bot')
CONTENT_RATING = 'g'  # Safe for everyone.
GIPHY_API_TOKEN = os.environ['GIPHY_API_TOKEN']
GIPHY_LOGO = 'https://i.ibb.co/nQwYkYy/Poweredby-100px-White-Vert-Logo.png'
GITLAB = 'https://gitlab.com'
GITLAB_API_TOKEN = os.environ['GITLAB_API_TOKEN']
LOG_LEVEL = os.environ.get('LOG_LEVEL', 'ERROR')
PORT = int(os.environ.get('PORT', 8080))
SEARCH_TERMS = ['amazing',
                'awesome',
                'congrats',
                'congratulations',
                'fantastic',
                'good job',
                'great job',
                'impressive',
                'lets celebrate',
                'well done',
                ]
SLACK_POST_HOOK = os.environ['SLACK_POST_HOOK']


def get_gif():
    """Load a random gif direct from Giphy."""

    giphy = giphy_client.DefaultApi()
    query = random.choice(SEARCH_TERMS)
    limit = 5
    try:
        response = giphy.gifs_search_get(GIPHY_API_TOKEN,
                                         query,
                                         limit=limit,
                                         offset=0,
                                         rating=CONTENT_RATING,
                                         lang='en',
                                         fmt='json')
        choice = random.choice(range(limit))
        return response.data[choice].images.downsized.url
    except ApiException as exn:
        print('Exception when calling DefaultApi->gifs_search_get: %s\n' % exn)


def post_on_slack(blocks):
    """Post a single message on Slack."""

    logging.debug('Sending: %s', repr(blocks))
    response = requests.post(SLACK_POST_HOOK,
                             headers={'Content-type': 'application/json'},
                             data=json.dumps(blocks))
    logging.debug('Response: %s', repr(response))


def get_author(author_id):
    """Get human readable username and full name from ID.

    Returns a pair of strings, e.g.:

        ('bc-bot', 'Bob the Beautiful Canoe Robot')

    We need to do this because user in the top-level of the payload is the name
    of the user who merged the MR. The other full name available is the name of
    the author of the last commit, but this may not be the person who was
    assigned  to close the related issue.
    """

    gitlab_dot_org = gitlab.Gitlab(GITLAB, GITLAB_API_TOKEN)
    user = gitlab_dot_org.users.get(author_id)
    return user.username.strip(), user.name.strip()


@BOT.router.register('Merge Request Hook', action='merge')
async def merge_request_merged_event(event, _gitlab, *_args, **_kwargs):
    """Whenever an MR is merged, congratulate the author.

    The message we construct here is in Slack block format:
    https://api.slack.com/block-kit

    Adding 'Powered by Giphy' is a condition of the API attribution policy:
    https://developers.giphy.com/docs/sdk/#design-guidelines
    """

    logging.debug('Received: %s', repr(event.data))
    gif = get_gif()
    # Format user information as a hyperlink to their GitLab user page.
    username, name = get_author(event.data['object_attributes']['author_id'])
    author = '<%s/%s|%s>' % (GITLAB, username, name)
    # Format the project name as a hyperlink to the GitLab project page.
    project = '<%s|%s>' % (event.data['project']['web_url'],
                           event.data['project']['name'])
    # Format the MR as a hyperlink to its GitLab page.
    mr_url = '<%s|%d>' % (event.data['object_attributes']['url'],
                          event.data['object_attributes']['iid'])
    # Format a message with more details about the MR. This will go near the
    # animated gif.
    mr_title = event.data['object_attributes']['title']
    mr_description = event.data['object_attributes']['description']
    message = (':canoe: *Congratulations* to %s on getting MR %s merged on '
               'project %s\n\n*%s*\n%s\n' %
               (author, mr_url, project, mr_title, mr_description))
    # Format the whole message as Slack blocks.
    blocks = {
        'blocks': [
            {
                'type': 'section',
                'text': {
                    'type': 'mrkdwn',
                    'text': message,
                }
            },
            {
                'type': 'divider'
            },
            {
                'type': 'image',
                'image_url': gif,
                'alt_text': 'Congratulations!'
            },
            {
                'type': 'divider'
            },
            {
                'type': 'image',
                'image_url': GIPHY_LOGO,
                'alt_text': 'Powered by GIPHY'
            },
            {
                'type': 'divider'
            }
        ]
    }
    post_on_slack(blocks)


logging.basicConfig(level=LOG_LEVEL)
logging.debug('About to start running on port %d.', PORT)


if __name__ == '__main__':
    BOT.run(port=PORT)
